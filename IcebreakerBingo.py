#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  7 16:52:44 2021

@author: simon
"""

from glob import glob
import os
import subprocess as SP
import random

class Sheet:
  def __init__(self, questionsFile='Questions.txt',
               rulesFile='Rules.txt'):
    self.qFl = questionsFile
    self.rFl = rulesFile
    self.LoadQuestions()
    self.LoadRules()
    
  def LoadQuestions(self):
    f = open(self.qFl, 'r')
    self.qLst = []
    for line in f:
      self.qLst.append(line[:-1])
    f.close()

  def LoadRules(self):
    self.rStr = open(self.rFl, 'r').read()

  def CreateSingleSheet(self, outputFileName='ExampleSheet.tex'):
    oFl = open(outputFileName, 'w')
    oFl.write(self.rStr)
    random.shuffle(self.qLst)
    for col in range(5):
      oStr = ""
      for row in range(5):
        oStr += '\\parbox[c]{2.9cm}{'+self.qLst[col*5 + row]+'} & '
      oFl.write(oStr[:-2])
      oFl.write('\\\\ \n')
      for i in range(2):
        oFl.write(' & & & & \\\\ \n')
      oFl.write('\\hline\n')
    oFl.write('\end{tabular}\n\end{document}')
    oFl.close()
  
def CompilePage(d='sheets', globString='IceBreaker*.tex'):
  os.chdir(d)
  for tFl in glob(globString):
    SP.run(['pdflatex', tFl], capture_output=True)
  os.chdir('..')
  
def ConcatPdf(globString='Icebreaker*.pdf'):
  cmd = 'pdftk'
  for pdf in glob(globString):
    cmd += f' {pdf}'
  cmd += ' cat output merged.pdf'
  SP.run(cmd.split())
  
if __name__=="__main__":
  s = Sheet()
  sDir = 'sheets'
  if not os.path.isdir(sDir):
    os.mkdir(sDir)
  for p in range(40):
    s.CreateSingleSheet(f'{sDir}/IceBreaker{p}.tex')
  
  print('Compiling sheets')  
  CompilePage(d=sDir, globString='IceBreaker*.tex')
  print('Concatenating sheets')
  ConcatPdf(globString=f'{sDir}/IceBreaker*.pdf')
  